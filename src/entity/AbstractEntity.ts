import { CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";

export abstract class AbstractEntity extends BaseEntity{
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column({default: true})
    ativo: boolean;

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: number;
}