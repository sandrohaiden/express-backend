import { Entity, Column } from "typeorm";
import { AbstractEntity } from "./AbstractEntity";
import { ApiModelProperty } from "swagger-express-ts";

@Entity()
export class AreaDeConhecimento extends AbstractEntity {

    @ApiModelProperty({required: true})
    @Column()
    nome: string;
}