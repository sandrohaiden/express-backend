import { Entity, Column, ManyToOne } from "typeorm";
import { AbstractEntity } from "./AbstractEntity";
import { GrupoDePesquisa } from "./GrupoDePesquisa";
import { Usuario } from "./Usuario";
import { FichaTecnica } from "./FichaTecnica";
import { ApiModelProperty } from "swagger-express-ts/api-model-property.decorator";

@Entity()
export class Produto extends AbstractEntity {
    @ApiModelProperty({required: true})
    @Column()
    titulo: string;

    @Column()
    site: string;

    @Column('json', {nullable: true})
    fotos;

    @Column('json', {nullable: true})
    paragrafos;

    @Column('json', {nullable: true})
    palavrasChave;

    @Column('json', {nullable: true})
    videos;

    @ManyToOne(type => GrupoDePesquisa, grupoDePesquisa => grupoDePesquisa.produtos)
    grupoDePesquisa: GrupoDePesquisa;

    @ManyToOne(type => Usuario, {nullable: true})
    proprietario: Usuario;

    @Column(type => FichaTecnica)
    fichaTecnica: FichaTecnica;
}