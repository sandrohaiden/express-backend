import { Column, ManyToOne } from "typeorm";
import { AreaDeConhecimento } from "./AreaDeConhecimento";

export class FichaTecnica{
    @Column({type: 'text', nullable: true})
    tituloOriginal: string;

    @Column({type: 'text', nullable: true})
    origem: string;

    @Column({type: 'text', nullable: true})
    publicoAlvo: string;

    @Column({type: 'varchar', length: 256, nullable: true})
    finalidade: string;

    @ManyToOne(type => AreaDeConhecimento, {nullable: true})
    areaDeConhecimento: AreaDeConhecimento;

    @Column({type: 'text', nullable: true})
    organizacao: string;

    @Column({type: 'text', nullable: true})
    registro: string;

    @Column({type: 'text', nullable: true})
    avaliacao: string;

    @Column({type: 'text', nullable: true})
    disponibilidade: string;

    @Column({type: 'text', nullable: true})
    divulgacao: string;

    @Column({type: 'text', nullable: true})
    financiamento: string;

    @Column({type: 'varchar', length: 256, nullable: true})
    idioma: string;

    @Column({type: 'varchar', length: 256, nullable: true})
    cidade: string;

    @Column({type: 'varchar', length: 256, nullable: true})
    pais: string;

    @Column({type: 'int', nullable: true})
    ano: string;
}