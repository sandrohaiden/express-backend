import { Entity, Column, ManyToOne, OneToMany } from "typeorm";
import { AbstractEntity } from "./AbstractEntity";
import { LinhaDePesquisa } from "./LinhaDePesquisa";
import { Produto } from "./Produto";
import { ApiModelProperty, SwaggerDefinitionConstant } from "swagger-express-ts";

@Entity()
export class GrupoDePesquisa extends AbstractEntity {
    @ApiModelProperty({required: true})
    @Column()
    nome: string;

    @ApiModelProperty({required: true})
    @Column()
    sigla: string;

    //@ApiModelProperty({model: "LinhaDePesquisa"})
    @ManyToOne(type => LinhaDePesquisa, linhaDePesquisa => linhaDePesquisa.gruposDePesquisa)
    linhaDePesquisa: LinhaDePesquisa;

    @OneToMany(type => Produto, produto => produto.grupoDePesquisa)
    @ApiModelProperty({model: "Produto", type: SwaggerDefinitionConstant.ARRAY})
    produtos: Produto[];
}