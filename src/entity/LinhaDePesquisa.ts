import { Entity, Column, OneToMany } from 'typeorm';
import { AbstractEntity } from './AbstractEntity';
import { GrupoDePesquisa } from './GrupoDePesquisa';
import { ApiModel, ApiModelProperty, SwaggerDefinitionConstant } from 'swagger-express-ts';

@Entity()
export class LinhaDePesquisa extends AbstractEntity {

    @ApiModelProperty( {
        required : true,
    } )
    @Column()
    nome: string;

    @ApiModelProperty()
    @Column({nullable: true})
    sigla: string;

    @OneToMany(type => GrupoDePesquisa, grupoDePesquisa => grupoDePesquisa.linhaDePesquisa)
    @ApiModelProperty({model: "GrupoDePesquisa", type: SwaggerDefinitionConstant.ARRAY})
    gruposDePesquisa: GrupoDePesquisa[];
}
