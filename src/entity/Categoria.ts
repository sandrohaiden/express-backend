import { Entity, Column } from "typeorm";
import { AbstractEntity } from "./AbstractEntity";
import { ApiModelProperty } from "swagger-express-ts/api-model-property.decorator";

@Entity()
export class Categoria extends AbstractEntity {

    @ApiModelProperty({required: true})
    @Column()
    nome: string;
}