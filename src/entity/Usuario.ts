import { Entity, Column, OneToMany, ManyToOne } from "typeorm";
import { AbstractEntity } from "./AbstractEntity";
import { GrupoDePesquisa } from "./GrupoDePesquisa";

@Entity()
export class Usuario extends AbstractEntity {
    
    @Column()
    nome: string;

    @Column()
    sobrenome: string;

    @Column()
    email: string;

    @Column()
    senha: string;

    @Column({nullable: true})
    lattes: string;

    @Column('json', {nullable: true})
    foto;

    @ManyToOne(type => GrupoDePesquisa, {nullable: true})
    grupoDePesquisa: GrupoDePesquisa;

    @OneToMany(type => Usuario, orientador => orientador.orientandos, {nullable: true})
    orientador: Usuario;

    @ManyToOne(type => Usuario, usuario => usuario.orientador)
    orientandos: Usuario[];

    @OneToMany(type => Usuario, coorientador => coorientador.coorientandos, {nullable: true})
    coorientador: Usuario;

    @ManyToOne(type => Usuario, usuario => usuario.coorientador)
    coorientandos: Usuario[];

    @Column({default: true})
    aluno: boolean;

    @Column({default: false})
    docente: boolean;

    @Column({default: false})
    coordenacao: boolean;

    @Column({default: false})
    externo: boolean;

    @Column({default: false})
    admin: boolean;
}