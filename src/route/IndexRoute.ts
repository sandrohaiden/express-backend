import { Router } from 'express';
import { LinhaDePesquisaController } from '../controller/LinhaDePesquisaController';
import { GrupoDePesquisaController } from '../controller/GrupoDePesquisaController';
import { AreaDeConhecimentoController } from '../controller/AreaDeConhecimentoController';

export class IndexRoute {
	readonly router = Router();
	constructor(app) {
        this.initializeControllers(app);
    }

	private initializeControllers(app) {
		app.use('/api/linhas-de-pesquisa', new LinhaDePesquisaController());
		app.use('/api/grupos-de-pesquisa', new GrupoDePesquisaController());
		app.use('/api/areas-de-conhecimento', new AreaDeConhecimentoController());
	}
}
