import { MoreThanOrEqual } from "typeorm";
import { NextFunction, Request, Response, Router } from "express";
import { GrupoDePesquisa } from "../entity/GrupoDePesquisa";
import { ApiPath, ApiOperationPost, SwaggerDefinitionConstant, ApiOperationGet, ApiModelProperty, ApiOperationDelete, ApiOperationPut } from "swagger-express-ts";
import { BULK_POST_OPERATION, GET_OPERATION, POST_OPERATION, DELETE_OPERATION, UPDATE_OPERATION } from "../utils.ts/SwaggerConsts";

const BASE_CLASS_NAME = 'GrupoDePesquisa';
const BASE_RESPONSE_CLASS_NAME = 'GrupoDePesquisaResponse';

class GrupoDePesquisaResponse {
    @ApiModelProperty({model: BASE_CLASS_NAME, type: SwaggerDefinitionConstant.ARRAY})
    data: GrupoDePesquisa[];

    @ApiModelProperty({type: SwaggerDefinitionConstant.INTEGER})
    count: number
}

@ApiPath({
    path: "/grupos-de-pesquisa",
    name: "Grupo de Pesquisa",
    security: { basicAuth: [] }
})
export class GrupoDePesquisaController {
    router: any;
    
    constructor(){
        this.router = Router();
        this.intializeRoutes()
        return this.router;
    }

    intializeRoutes() {
        this.router.get('/', this.all);
        this.router.post('/', this.save);
        this.router.put('/:id', this.update);
        this.router.delete('/:id', this.remove);
        this.router.post('/bulk', this.saveBulk);
    }

    @ApiOperationGet(GET_OPERATION(BASE_RESPONSE_CLASS_NAME))
    async all(request: Request, response: Response, next: NextFunction) {
        try {
            const [result, total] = await GrupoDePesquisa.getRepository().findAndCount({take: request.query.take, skip: request.query.skip});
            response.send({content: result, totalElements: total});
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationPost(POST_OPERATION(BASE_CLASS_NAME))
    async save(request: Request, response: Response, next: NextFunction) {
        try {
            response.send(GrupoDePesquisa.getRepository().save(request.body));
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationPut(UPDATE_OPERATION(BASE_CLASS_NAME))
    async update(request: Request, response: Response, next: NextFunction) {
        try {
            await GrupoDePesquisa.getRepository().findOneOrFail(request.params.id);
            response.send(GrupoDePesquisa.getRepository().update(request.params.id, request.body));
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationDelete(DELETE_OPERATION(BASE_CLASS_NAME))
    async remove(request: Request, response: Response, next: NextFunction) {
        try {
            let removed = await GrupoDePesquisa.getRepository().findOneOrFail(request.params.id);
            await GrupoDePesquisa.getRepository().remove(removed);
            response.send(removed);
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationPost(BULK_POST_OPERATION(BASE_CLASS_NAME))
    async saveBulk(request: Request, response: Response, next: NextFunction) {
        try {
            const result = await GrupoDePesquisa.createQueryBuilder()
            .insert().into(GrupoDePesquisa).values(request.body).execute();
            response.send(
                GrupoDePesquisa.getRepository().find({where: {id: MoreThanOrEqual(result.raw.insertId)}})
            );
        } catch (error) {
            response.send(error);
        }
    }

}