import { MoreThanOrEqual } from "typeorm";
import { NextFunction, Request, Response, Router } from "express";
import { LinhaDePesquisa } from "../entity/LinhaDePesquisa";
import { ApiPath, ApiOperationPost, SwaggerDefinitionConstant, ApiOperationGet, ApiModelProperty, ApiOperationDelete, ApiOperationPut } from "swagger-express-ts";
import { BULK_POST_OPERATION, GET_OPERATION, POST_OPERATION, DELETE_OPERATION, UPDATE_OPERATION } from "../utils.ts/SwaggerConsts";

const BASE_CLASS_NAME = 'LinhaDePesquisa';
const BASE_RESPONSE_CLASS_NAME = 'LinhaDePesquisaResponse'

class LinhaDePesquisaResponse {
    @ApiModelProperty({model: BASE_CLASS_NAME, type: SwaggerDefinitionConstant.ARRAY})
    data: LinhaDePesquisa[];

    @ApiModelProperty({type: SwaggerDefinitionConstant.INTEGER})
    count: number
}

@ApiPath({
    path: "/linhas-de-pesquisa",
    name: "Linha de Pesquisa",
    security: { basicAuth: [] }
})
export class LinhaDePesquisaController {
    router: any;
    
    constructor(){
        this.router = Router();
        this.intializeRoutes()
        return this.router;
    }

    intializeRoutes() {
        this.router.get('/', this.all);
        this.router.post('/', this.save);
        this.router.put('/:id', this.update);
        this.router.delete('/:id', this.remove);
        this.router.post('/bulk', this.saveBulk);
    }

    @ApiOperationGet(GET_OPERATION(BASE_RESPONSE_CLASS_NAME))
    async all(request: Request, response: Response, next: NextFunction) {
        try {
            const [result, total] = await LinhaDePesquisa.getRepository().findAndCount({take: request.query.take, skip: request.query.skip});
            response.send({content: result, totalElements: total});
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationPost(POST_OPERATION(BASE_CLASS_NAME))
    async save(request: Request, response: Response, next: NextFunction) {
        try {
            response.send(LinhaDePesquisa.getRepository().save(request.body));
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationPut(UPDATE_OPERATION(BASE_CLASS_NAME))
    async update(request: Request, response: Response, next: NextFunction) {
        try {
            await LinhaDePesquisa.getRepository().findOneOrFail(request.params.id);
            response.send(LinhaDePesquisa.getRepository().update(request.params.id, request.body));
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationDelete(DELETE_OPERATION(BASE_CLASS_NAME))
    async remove(request: Request, response: Response, next: NextFunction) {
        try {
            let removed = await LinhaDePesquisa.getRepository().findOneOrFail(request.params.id);
            await LinhaDePesquisa.getRepository().remove(removed);
            response.send(removed);
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationPost(BULK_POST_OPERATION(BASE_CLASS_NAME))
    async saveBulk(request: Request, response: Response, next: NextFunction) {
        try {
            const result = await LinhaDePesquisa.createQueryBuilder()
            .insert().into(LinhaDePesquisa).values(request.body).execute();
            response.send(
                LinhaDePesquisa.getRepository().find({where: {id: MoreThanOrEqual(result.raw.insertId)}})
            );
        } catch (error) {
            response.send(error);
        }
    }

}