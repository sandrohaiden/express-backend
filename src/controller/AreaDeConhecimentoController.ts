import { MoreThanOrEqual } from "typeorm";
import { NextFunction, Request, Response, Router } from "express";
import { AreaDeConhecimento } from "../entity/AreaDeConhecimento";
import { ApiPath, ApiOperationPost, SwaggerDefinitionConstant, ApiOperationGet, ApiModelProperty, ApiOperationDelete, ApiOperationPut } from "swagger-express-ts";
import { BULK_POST_OPERATION, GET_OPERATION, POST_OPERATION, DELETE_OPERATION, UPDATE_OPERATION } from "../utils.ts/SwaggerConsts";

const BASE_CLASS_NAME =  'AreaDeConhecimento';
const BASE_RESPONSE_CLASS_NAME =  'AreaDeConhecimentoResponse';
class AreaDeConhecimentoResponse {
    @ApiModelProperty({model: "AreaDeConhecimento", type: SwaggerDefinitionConstant.ARRAY})
    data: AreaDeConhecimento[];

    @ApiModelProperty({type: SwaggerDefinitionConstant.INTEGER})
    count: number
}

@ApiPath({
    path: "/areas-de-conhecimento",
    name: "Área de Conchecimento",
    security: { basicAuth: [] }
})
export class AreaDeConhecimentoController {
    router: any;
    
    constructor(){
        this.router = Router();
        this.intializeRoutes()
        return this.router;
    }

    intializeRoutes() {
        this.router.get('/', this.all);
        this.router.post('/', this.save);
        this.router.put('/:id', this.update);
        this.router.delete('/:id', this.remove);
        this.router.post('/bulk', this.saveBulk);
    }

    @ApiOperationGet(GET_OPERATION(BASE_RESPONSE_CLASS_NAME))
    async all(request: Request, response: Response, next: NextFunction) {
        try {
            const [result, total] = await AreaDeConhecimento.getRepository().findAndCount({take: request.query.take, skip: request.query.skip});
            response.send({content: result, totalElements: total});
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationPost(POST_OPERATION(BASE_CLASS_NAME))
    async save(request: Request, response: Response, next: NextFunction) {
        try {
            response.send(AreaDeConhecimento.getRepository().save(request.body));
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationPut(UPDATE_OPERATION(BASE_CLASS_NAME))
    async update(request: Request, response: Response, next: NextFunction) {
        try {
            await AreaDeConhecimento.getRepository().findOneOrFail(request.params.id);
            response.send(AreaDeConhecimento.getRepository().update(request.params.id, request.body));
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationDelete(DELETE_OPERATION(BASE_CLASS_NAME))
    async remove(request: Request, response: Response, next: NextFunction) {
        try {
            let removed = await AreaDeConhecimento.getRepository().findOneOrFail(request.params.id);
            await AreaDeConhecimento.getRepository().remove(removed);
            response.send(removed);
        } catch (error) {
            response.send(error);
        }
    }

    @ApiOperationPost(BULK_POST_OPERATION(BASE_CLASS_NAME))
    async saveBulk(request: Request, response: Response, next: NextFunction) {
        try {
            const result = await AreaDeConhecimento.createQueryBuilder()
            .insert().into(AreaDeConhecimento).values(request.body).execute();
            response.send(
                AreaDeConhecimento.getRepository().find({where: {id: MoreThanOrEqual(result.raw.insertId)}})
            );
        } catch (error) {
            response.send(error);
        }
    }

}