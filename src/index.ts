import 'reflect-metadata';
import { createConnection } from 'typeorm';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as logger from 'morgan';
import * as swagger from 'swagger-express-ts';
import * as swaggerUi from 'swagger-ui-express';
import * as cors from 'cors';
import { IndexRoute } from './route/IndexRoute';

createConnection()
	.then(async connection => {
		// create express app
		const app = express();

		app.use(bodyParser.json());
		app.use(logger('dev'));
		app.use(cors());

		// setup express app here
		// ...
        new IndexRoute(app);
		app.use(
			swagger.express({
				definition: {
					info: {
						title: 'Vitrine Digital - Backend',
						version: '1.0'
					},
					host: 'localhost:3000',
					basePath: '/api',
					produces: ['application/json'],
					schemes: ['http', 'https'],
					securityDefinitions: {
						JWT: {
							type: 'apiKey',
							in: 'header',
							name: 'Authorization'
						}
					},
					externalDocs: {
						url: 'My url'
					}
					// Models can be defined here
				}
			})
		);
		var options = {
			swaggerOptions: {
				url: 'http://localhost:3000/api-docs/swagger.json'
			}
		};
		app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, options));
		// start express server
		app.listen(3000);

		console.log('Server http://localhost:3000/api is up.');
		console.log('The documentation is acessible in http://localhost:3000/api-docs')
	})
	.catch(error => console.log(error));
