import { SwaggerDefinitionConstant } from "swagger-express-ts";

const QUERY_PARAMS= {
    take: {
        allowEmptyValue: true, 
        type: SwaggerDefinitionConstant.INTEGER, 
        description: "Número de resultados a serem retornados por consulta."
    },
    skip: {
        allowEmptyValue: true, 
        type: SwaggerDefinitionConstant.INTEGER, 
        description: "Número de resultados que devem ser ignorados na consulta atual."
    }
}

function GET_RESPONSE(model: string): any{
    return{
        200: { description: "Success", type: SwaggerDefinitionConstant.Response.Type.ARRAY, model: model},
        400: { description: "Parameters fail" }
    }
}

function POST_RESPONSE(model: string): any{
    return{
        200: { description: "Success", model: model},
        400: { description: "Parameters fail" }
    }
}

function BULK_POST_RESPONSE(model: string): any{
    return{
        200: { description: "Success", model: model},
        400: { description: "Parameters fail" }
    }
}

function BULK_POST_OPERATION(model: string): any {
    return {
        path: "/bulk",
        description: `Salva vários objetos de ${model} recebidos pelo body`,
        summary: "Salvar em massa",
        parameters: {
            body: { description: `Array de objetos ${model}`, required: true, type: SwaggerDefinitionConstant.ARRAY, model: model }
        },
        responses: BULK_POST_RESPONSE(model)
    }
}

function GET_OPERATION(model: string): any {
    return {
        description: `Retorna uma listagem e quantidade de todos os registros de ${model}, tem suporte a paginação por String Query com take e skip`,
        summary: `Retorna ${model} com paginação`,
        parameters: {
            query: QUERY_PARAMS
        },
        responses: GET_RESPONSE(model)
    }
}

function POST_OPERATION(model: string): any {
    return {
        description: `Efetua o registro de ${model} no banco de dados`,
        summary: `Salva um registro de ${model}`,
        parameters: {
            body: { description: `Um objeto do tipo ${model}`, required: true, model: model }
        },
        responses: POST_RESPONSE(model)
    }
}

function DELETE_OPERATION(model: string): any {
    return {
        path: "/:id",
        description: `Efetua a exclusão de um registro de ${model} no banco de dados`,
        summary: `Exclui um registro de ${model}`,
        parameters : {
            path : {
                id : {
                    description : `Id de ${model}`,
                    type : SwaggerDefinitionConstant.Parameter.Type.STRING,
                    required : true
                }
            }
        },
        responses: POST_RESPONSE(model)
    }
}

function UPDATE_OPERATION(model: string): any {
    return {
        path: "/:id",
        description: `Efetua a atualização de um registro de ${model} no banco de dados`,
        summary: `Exclui um registro de ${model}`,
        parameters : {
            path : {
                id : {
                    description : `Id de ${model}`,
                    type : SwaggerDefinitionConstant.Parameter.Type.STRING,
                    required : true
                }
            },
            body: { description: `Um objeto do tipo ${model}`, required: true, model: model }
        },
        responses: POST_RESPONSE(model)
    }
}

export { BULK_POST_OPERATION, GET_OPERATION, POST_OPERATION, DELETE_OPERATION, UPDATE_OPERATION }